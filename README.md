# Demo of SDL/Cairo/OpenGL app framework in Haskell for Android & desktop.

## Also demonstrates how to build Haskell mobile (Android) apps using [ghc-cross-compile](https://gitlab.com/alleycatcc/ghc-cross-compile) and [haskell-android-sdl](https://gitlab.com/alleycatcc/haskell-android-sdl).

## See also [sdl-gles-cairo-flashlight](https://gitlab.com/alleycatcc/sdl-gles-cairo-flashlight)

----

## Init:

    copy vars-plugin.sh-example vars-plugin.sh

## To build the Android .apk:

-   Init build.

        bin/build build

-   Connect the device via USB. Make sure it’s in ‘developer mode’.

-   Finish build and push.

        bin/docker [-m /some/dir] build-apk-and-deploy

-   The `-m` option will mount `/some/dir` as a shared folder in the
    docker container and copy the built .apk there in case you want to save
    it. With or without `-m`, the app will be pushed to the device.

-   When the device shows an ok/cancel dialog try to accept immediately
    (the window is very short). If you’re too late, it will fail, but then
    you only just need to repeat the last step.

-   If the device isn’t being found by the script, check if you have an
    `adb` process running.

        killall adb

-   If all goes well the app will show up in the drawer with an icon of a
    fish. The text of the icon is the value in `vars-plugin.sh`.

-   When you run it, it will probably complain about text relocations and/or
    old APIs, but you should be able to bypass the warning and run it anyway.

-   Make changes to the source and do it again.

    -   Remember to either uninstall the old app from the phone, or to
        change the number at the end of the Java package name in
        `vars-plugin.sh`

    -   Check the changes first by running the desktop app (see below). Just
        remember that the Android version does not touch the filesystem at
        all. So instead of changing `config.yaml`, change
        `Graphics/SGCDemo/ConfigInline.hs`.

## To build the desktop app:

-   Add our cabal repository to your cabal config (e.g. `~/.cabal/config`).
    This is so you can depend on our `sdl-gles-cairo`, `sdl-cairo-demo-cat`
    and `mesh-obj-gles` packages. (The Android build does not use this
    repository, by the way: it is using the old-style cabal and just
    includes the dependency as a directory.)

        repository cabal.alleycat.cc
          url: https://cabal.alleycat.cc/

-   Then:

        cd sdl-gles-cairo-demo
        cabal update
        # --- use -p if you want to analyse the performance.
        cabal configure -fbuild-executable [-p]
        cabal run

-   You can ignore the error about ‘udev_device_get_action’ if you see it.

-   Try out different values in `config.yaml`.

-   Known to work with GHC 8.6.5 and cabal-install 3.0.0.0 on GNU/Linux (amd64).
