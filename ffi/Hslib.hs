module Hslib where

import Control.Monad ( (<=<) )
import Foreign.C ( CString, CInt (..), newCString )

import           Graphics.SGCDemo.Launch ( launch )

foreign export ccall launchit :: CInt -> CInt -> IO ()

foreign import ccall "android_info" androidInfo   :: CString -> IO ()
foreign import ccall "android_warn" androidWarn   :: CString -> IO ()
foreign import ccall "android_error" androidError :: CString -> IO ()

launchit :: CInt -> CInt -> IO ()
launchit width height = launch (androidInfo', androidWarn', androidError') dims where
    androidInfo'  = androidInfo  <=< newCString
    androidWarn'  = androidWarn  <=< newCString
    androidError' = androidError <=< newCString
    dims = Just (frint width, frint height)

frint = fromIntegral
