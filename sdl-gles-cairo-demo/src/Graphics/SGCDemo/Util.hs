{-# LANGUAGE PackageImports #-}

module Graphics.SGCDemo.Util ( benchStart
                             , benchUpdate
                             , inv
                             , printMatrixDouble
                             , color
                             , col8
                             , color4
                             , color3
                             , randoms
                             , hsvCycle
                             , replicateX
                             , nAtATime
                             , glTrue
                             , glFalse
                             , glTrueF
                             , glFalseF
                             ) where

import           Prelude hiding ( log )

import           Data.List ( unfoldr )
import           Data.Time ( UTCTime, diffUTCTime, getCurrentTime )
import           Data.Function ( (&) )
import qualified Data.StateVar      as STV ( get )
import           Data.Foldable ( find , foldl' )
import           Data.Word ( Word8 )
import           Data.Monoid ( (<>) )
import           Text.Printf                  ( printf )
import           Foreign                      ( peekElemOff )
import           Foreign.C                    ( peekCString )
import           System.Random                ( getStdGen )
import qualified System.Random as SR          ( randoms )

import           Graphics.Rendering.OpenGL as GL
                 ( Vector3 ( Vector3 )
                 , Vector4 ( Vector4 )
                 , Color4 (Color4)
                 , Color3 (Color3)
                 , GLint
                 , GLfloat
                 , Vertex1 ( Vertex1 )
                 , Vertex2 ( Vertex2 )
                 , Vertex3 ( Vertex3 )
                 , Vertex4 ( Vertex4 )
                 , withMatrix
                 , errors )

import           Graphics.SDLGles.Types ( Log (info, err)
                                        , App
                                        , appMatrix
                                        )

import           Graphics.SDLGles.Util2 ( multMatrices, frint )
import           Graphics.SDLGles.Types ( DMat
                                        , GMatD )
import           Graphics.SGCDemo.Config ( doDebug
                                         , doBench )

data Bench = BenchDisabled
           | BenchEnabled { benchLog :: Log
                          , benchTag :: String
                          , benchStartTime :: UTCTime }

debug | doDebug == True = info
      | otherwise       = const . const . pure $ ()

-- | return stream of random doubles from [-1, 1]
-- • same stream returned per process, possible edge/distribution effects.
randoms :: IO [Double]
randoms = do
    gen <- getStdGen
    return . map divider' . SR.randoms $ gen where
        divider' :: Int -> Double
        divider' n = fromIntegral n / fromIntegral (maxBound :: Int)

-- | good for pushing to vertices, although in the shaders we use floats.
col8 :: Integral a => a -> a -> a -> a -> (Word8, Word8, Word8, Word8)
col8 r g b a = (c r, c g, c b, c a) where
    c = frint

-- | constuct their Color3/Color4 (Float) types with integral input.
color4 :: Integral a => a -> a -> a -> a -> Color4 Float
color4 r g b a = Color4 (c r) (c g) (c b) (c a) where
    c = (/ 255) . frint

color3 :: Integral a => a -> a -> a -> Color3 Float
color3 r g b = Color3 (c r) (c g) (c b) where
    c = (/ 255) . frint

color :: Integral a => a -> a -> a -> a -> Vertex4 Float
color r g b a = Vertex4 (x r) (x g) (x b) (x a) where
    x = (/ 255) . frint

benchStart log tag | doBench == False = pure BenchDisabled
benchStart log tag | doBench == True = do
    t <- getCurrentTime
    pure $ BenchEnabled { benchLog = log
                        , benchTag = tag
                        , benchStartTime = t }

benchUpdate bench | doBench == False = pure ()
benchUpdate bench | doBench == True = do
    t            <- getCurrentTime
    now'         <- getCurrentTime
    let tag'      = benchTag bench
        then'     = benchStartTime bench
        log'      = benchLog bench
        elapsed'  = show $ diffUTCTime now' then'
    info log' $ printf "«%s» elapsed: %s" tag' elapsed'

printMatrixDouble :: String -> GMatD -> IO ()
printMatrixDouble tag matrix = withMatrix matrix peek' where
    peek' order src = mapM_ (map' order src) [0 .. 15]
    map' order src' n = do
        val' <- peekElemOff src' n
        let put' | n == 0     = putStr   $ printf "%s (%s) => %.1f " tag (show order) val'
                 | n == 8     = putStrLn $ printf "%.1f " val'
                 | otherwise  = putStr   $ printf "%.1f " val'
        put'

hsvCycle :: Int -> Int -> (Int, Int, Int)
hsvCycle total tt
    | phase == 0 = (255, asc, 0)
    | phase == 1 = (desc, 255, 0)
    | phase == 2 = (0, 255, asc)
    | phase == 3 = (0, desc, 255)
    | phase == 4 = (asc, 0, 255)
    | phase == 5 = (255, 0, desc) where

    t = tt `mod` total
    asc  = floor $ frint x / frint tp * 255
    desc = 255 - asc
    tp = floor $ frint total / 6
    (phase, x) | t < tp * 1 = (0, t)
               | t < tp * 2 = (1, t `mod` (tp * 1))
               | t < tp * 3 = (2, t `mod` (tp * 2))
               | t < tp * 4 = (3, t `mod` (tp * 3))
               | t < tp * 5 = (4, t `mod` (tp * 4))
               | otherwise  = (5, t `mod` (tp * 5))

replicateX n x = unfoldr unfold' (x, 0) where
    unfold' (v, m) | m == n = Nothing
                   | otherwise = Just (v, (v, m + 1))

-- alternate implementation.
-- replicateX n x = concat . replicate n $ [x]

glFalse = 0 :: GLint
glTrue  = 1 :: GLint

glFalseF = 0 :: GLfloat
glTrueF  = 1 :: GLfloat


-- | if it's not evenly divisible, the last element will be shorter.
nAtATime n xs
  | length right' == 0 = [left']
  | otherwise = left' : tail' where
    tail' = nAtATime n right'
    (left', right') = splitAt n xs

inv x = (-1) * x
