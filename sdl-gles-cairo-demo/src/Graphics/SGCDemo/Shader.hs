module Graphics.SGCDemo.Shader ( initShaderColor
                               , initShaderTexture
                               , initShaderMesh
                               , initShaderMeshScene ) where

import           Data.Monoid ( (<>) )
import qualified Data.ByteString.Char8  as BS8 ( pack )
import qualified Data.StateVar        as STV ( get )

import           Graphics.Rendering.OpenGL as GL
                 ( AttribLocation
                 , UniformLocation
                 , attribLocation
                 , uniformLocation )

import           Graphics.SDLGles.Types
                 ( Shader (Shader) )

import           Graphics.SGCDemo.Config
                 ( isEmbedded )

import           Graphics.SGCDemo.Types ( Shader' (Shader'C, Shader'T, Shader'M)
                                        , mkShaderVarsC
                                        , mkShaderVarsT
                                        , mkShaderVarsM
                                        , mkShaderVarsMS
                                        , mkMatrix
                                        )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SDLGles.GLES.Shader ( initProgram )

import qualified Graphics.SGCDemo.ShaderSrc.VertexColor as SVC ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.FragmentColor as SFC ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.FragmentColor as SFC ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.VertexTexture as SVT ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.FragmentTexture as SFT ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.VertexMesh as SVM ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.FragmentMesh as SFM ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.VertexMeshScene as SVMS ( shaderSrc )
import qualified Graphics.SGCDemo.ShaderSrc.FragmentMeshScene as SFMS ( shaderSrc )

-- useful for developing shaders; otherwise keep to false because the FS
-- version and inline versions easily get out of sync.
allowFileSystemShaders = False

getShader = BS8.pack

initShaderColor log =
    let vShader = getShader SVC.shaderSrc
        fShader = getShader SFC.shaderSrc
    in  Shader'C <$> initShader log "c" vShader fShader

initShaderTexture log =
    let vShader = getShader SVT.shaderSrc
        fShader = getShader SFT.shaderSrc
    in  Shader'T <$> initShader log "t" vShader fShader

initShaderMesh log =
    let vShader = getShader SVM.shaderSrc
        fShader = getShader SFM.shaderSrc
    in  Shader'M <$> initShader log "m" vShader fShader

initShaderMeshScene log =
    let vShader = getShader SVMS.shaderSrc
        fShader = getShader SFMS.shaderSrc
    in  Shader'M <$> initShader log "ms" vShader fShader

initShader log shaderType vShaderSrc fShaderSrc = do
    prog' <- initProgram log vShaderSrc fShaderSrc
    let unif' str     = wrapGL log ("uniformLocation " <> str) . STV.get $ uniformLocation prog' str
        att' str      = wrapGL log ("attribLocation "  <> str) . STV.get $ attribLocation  prog' str
    let shaderVars'   = case shaderType of "t"  -> mkShaderVarsT
                                           "c"  -> mkShaderVarsC
                                           "m"  -> mkShaderVarsM
                                           "ms"  -> mkShaderVarsMS
    Shader <$> pure prog' <*> mkMatrix log prog' <*> shaderVars' log prog'

-- | no 'enable' necessary for uniforms.
-- • attribs need to be enabled / disabled when drawArrays is called.
-- • remember to 'use' the program in the render loop.

getShadersFilesystem = do
    vShaderColor   <- BS8.pack <$> readFile "vertex-color"
    fShaderColor   <- BS8.pack <$> readFile "fragment-color"

    vShaderTextureFaces <- BS8.pack <$> readFile "vertex-texture-faces"
    fShaderTextureFaces <- BS8.pack <$> readFile "fragment-texture-faces"

    vShaderMesh <- BS8.pack <$> readFile "vertex-mesh"
    fShaderMesh <- BS8.pack <$> readFile "fragment-mesh"

    vShaderMeshScene <- BS8.pack <$> readFile "vertex-mesh-scene"
    fShaderMeshScene <- BS8.pack <$> readFile "fragment-mesh-scene"

    pure ( vShaderColor, fShaderColor
         , vShaderTextureFaces, fShaderTextureFaces
         , vShaderMesh, fShaderMesh
         , vShaderMeshScene, fShaderMeshScene )
