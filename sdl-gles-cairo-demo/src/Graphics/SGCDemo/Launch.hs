{-# LANGUAGE OverloadedStrings #-}

{-# LANGUAGE PackageImports #-}
{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# LANGUAGE BlockArguments #-}

module Graphics.SGCDemo.Launch ( launch, launchWithArgs ) where

import           Prelude hiding ( log, init )

import           Numeric ( readHex )
import           Data.Bits ( (.&.), shiftR )
import qualified Data.ByteString    as BS ( readFile )
import           Data.Text ( Text )
import qualified Data.StateVar      as STV ( get )
import           Data.List ( zip4, findIndex, zip5 )
import           Debug.Trace ( trace )
import           Data.Function ( (&) )
import           Data.Maybe ( isJust, fromJust )
import           Data.Either ( isLeft )
import           Data.ByteString    as BS ( ByteString )
import           Control.Applicative ( (<|>), empty )
import           Data.Monoid ( (<>) )
import           Foreign ( Ptr
                         , free
                         , peekElemOff
                         , mallocArray )
import           Text.Printf ( printf )
import           Control.Monad ( when, unless, forM_, forM, join )
import           Data.Map as Dmap ( Map
                                  , toList )
import           Control.DeepSeq ( deepseq )
import qualified Data.Yaml as Y ( decodeEither' )
import qualified Data.Vector          as DV  ( toList )

import qualified Data.ByteString.Base64 as B64 ( decode )

import           Data.Stack ( stackNew )

import "matrix"  Data.Matrix        as DMX ( (!) )

import           Codec.Picture      as JP ( decodePng )

import           SDL as S
                 ( ($=)
                 , Mode (Normal)
                 , Profile (Compatibility, ES)
                 , initializeAll
                 , createWindow
                 , defaultWindow
                 , defaultOpenGL
                 , glColorPrecision
                 , glDepthPrecision
                 , glStencilPrecision
                 , glMultisampleSamples
                 , glProfile
                 , glCreateContext
                 , glSwapWindow )

import           Graphics.Rendering.OpenGL as GL
                 ( Color4 (Color4)
                   -- no constructors, just synonym for Int32
                 , GLsizei
                 , TextureObject (TextureObject)
                 , ShadingModel ( Smooth )
                 , PixelStoreDirection ( Unpack, Pack )
                 , ClearBuffer ( ColorBuffer, DepthBuffer )
                 , Vertex2 ( Vertex2 )
                 , Vertex3 ( Vertex3 )
                 , Vertex4 ( Vertex4 )
                 , PrimitiveMode ( Triangles, TriangleFan )
                 , PixelFormat ( DepthComponent )
                 , Position ( Position )

                  -- Not Ptr CUChar <that's the *Cairo* PixelData>
                 , PixelData ( PixelData )
                 , DataType ( Float )
                 , Capability ( Enabled, Disabled )
                 , TextureFilter ( Nearest )
                 , Size ( Size )
                 , GLfloat
                 , Vector3 ( Vector3 )
                 , Vector4 ( Vector4 )
                 , HintTarget (PolygonSmooth)
                 , HintMode (Nicest)
                 , BlendingFactor ( One, OneMinusSrcAlpha, SrcAlpha, SrcAlphaSaturate )
                 , PixelInternalFormat ( RGBA'
                                       , RGBA8
                                       , RGB8
                                       , RGB' -- GL_RGB
                                       )
                 , hint
                 , drawArrays
                 , genObjectNames
                 , viewport
                 , readPixels
                 , rowAlignment
                 , textureFilter
                 , blend
                 , blendFunc
                 , depthFunc
                 , clearColor )

import qualified Graphics.Rendering.OpenGL as GL
                 ( clear )

import qualified Graphics.Rendering.Cairo as C
                 ( Render
                 , Operator (OperatorClear, OperatorSource)
                 -- only exported for certain cairo versions <see source>
                 -- formatStrideForWidth
                 , setOperator
                 , renderWith
                 , translate
                 , save
                 , restore
                 , transform
                 , rotate
                 , setLineWidth
                 , setSourceRGBA
                 , moveTo
                 , rectangle
                 , fill
                 , arc
                 , stroke )

-- sdl-cairo-demo-cat
import           Graphics.DemoCat.Render.Render     as SCC ( renderFrames )

-- mesh-obj-gles
import qualified Codec.MeshObjGles.Parse as Cmog
                 ( Config (Config)
                 , ConfigObjectSpec (ConfigObjectSpec)
                 , ConfigObjectSpecItem (ConfigObjectFilePath, ConfigObjectSource)
                 , ConfigMtlSpec (ConfigMtlFilePath, ConfigMtlSource)
                 , TextureConfig (TextureConfig)
                 , Sequence (Sequence)
                 , SequenceFrame (SequenceFrame)
                 , Texture (Texture)
                 , Burst (Burst)
                 , Vertices
                 , TexCoords
                 , TextureMap
                 , ObjName
                 , MtlName
                 , Normals
                 , Vertex2 (Vertex2)
                 , Vertex3 (Vertex3)
                 , TextureTypesSet
                 , TextureType
                 ( TextureDiffuse, TextureAmbient
                 , TextureDissolve, TextureSpecular
                 , TextureSpecularExp, TextureEmissive )
                 , materialTexture
                 , materialSpecularExp
                 , materialAmbientColor
                 , materialDiffuseColor
                 , materialSpecularColor
                 , materialTextureTypes
                 , makeInfiniteSequence
                 , tailSequence
                 , tcImageBase64
                 , tcWidth
                 , tcHeight
                 , parse )

import           Graphics.SGCDemo.ImageData
                 ( imageNefeli
                 , imageNeske
                 , imageHarlem
                 , imageShigeru
                 , imageFence
                 , imageLondon
                 , imageViking
                 , imagePlaza1
                 , imagePlaza2
                 , imagePlaza3
                 , imagePlaza4
                 , imageGalerie4
                 , imageGalerie5
                 , imageGalerie6
                 , imageGalerie7
                 , imageGalerie8
                 , imageGalerie9
                 , imageGalerie10
                 , imageWolf
                 , movieBat
                 , movieCaspian )

import           Graphics.SGCDemo.Shader
                 ( initShaderColor
                 , initShaderTexture
                 , initShaderMesh
                 , initShaderMeshScene )

import           Graphics.SDLGles.GLES.Draw
                 ( pushColors
                 , rectangle
                 , rectangleTex
                 , triangle
                 , sphere
                 , cylinder
                 , cylinderTex
                 , circle
                 , coneSection
                 , coneSectionTex
                 , pushPositions
                 , pushPositionsWithArray
                 , pushTexCoords
                 , pushTexCoordsWithArray
                 , pushNormals
                 , pushNormalsWithArray
                 , pushAttributesVertex4
                 , pushAttributesFloat
                 , pushAttributesWithArrayVertex4
                 , pushAttributesWithArrayScalar
                 , rectangleStroke
                 )

import           Graphics.SDLGles.GLES.Coords
                 ( vec3
                 , verl3
                 , vec4
                 , vec3gld
                 , invertMajor'
                 , ver3
                 , ver3gld
                 , frustumF
                 , orthoF
                 , lookAtF
                 , normalize
                 , unProjectItF
                 , rotateX
                 , rotateY
                 , rotateZ
                 , scaleX
                 , scaleY
                 , scaleZ
                 , verple3
                 , invertMatrix
                 , identityMatrix
                 , multMatrices
                 , vecl4
                 , toMGC
                 , toMGCD
                 , translateX
                 , translateY
                 , translateZ
                 , ver3
                 , ver4
                 , vec3 )

import           Graphics.SDLGles.GLES.Shader
                 ( uniform
                 , useShader
                 , attrib )

import           Graphics.SDLGles.Util
                 ( appReplaceModel
                 , appUpdateMatrix
                 , appMultiplyModel
                 , appMultiplyRightModel
                 , appMultiplyView
                 , replaceModel
                 , replaceView
                 , replaceProj
                 , pushModel
                 , pushView
                 , pushProj
                 , stackPop'
                 , stackReplace' )

import           Graphics.SDLGles.Util2
                 ( frint )

import           Graphics.SDLGles.Util3
                 ( map3
                 , deg2rad
                 , vcross
                 , vmag
                 , v3x
                 , v3y
                 , v3z
                 , vdot
                 , vdiv
                 , float
                 , toDeg
                 , fst3
                 , snd3
                 , thd3 )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SGCDemo.Util
                 ( hsvCycle
                 , nAtATime
                 , glTrueF
                 , glFalseF
                 , color
                 , col8
                 , color4
                 , color3
                 , inv
                 , randoms
                 )

import           Graphics.SDLGles.Texture
                 ( createTextures2DSimple
                 , activateTexture
                 , updateTexture
                 , updateTextures
                 , updateTextureCairo
                 , textureWithCairo
                 , textureNoCairo )

import           Graphics.SDLGles.Config
                 ( defaultConfig
                 )

import Graphics.SDLGles.Types
    ( App(App),
      Log(Log, info, warn, err),
      Logger,
      appConfig,
      appLog,
      appMatrix,
      appUser,
      output565,
      toShaderDC,
      toShaderDT,
      windowWidth,
      windowHeight,
      GraphicsData(GraphicsSingle, GraphicsSingleCairo, GraphicsMoving),
      ShaderD(ShaderDC, ShaderDT),
      Tex(NoTexture),
      GraphicsTextureMapping(GraphicsTextureMapping),
      ProjectionType(ProjectionFrustum, ProjectionOrtho),
      getCSurf,
      graphicsTextureMappingGraphicsData,
      graphicsTextureMappingTexture,
      graphicsTextureMappingTextureObject,
      mvpConfigProjectionType,
      mvpConfigCubeScale,
      mvpConfigTranslateZ,
      attribAttribLocation,
      uniformUniformLocation,
      texWidth,
      texHeight,
      shaderDCProgram,
      shaderDCUniformModel,
      shaderDCUniformView,
      shaderDCUniformProjection,
      shaderDCAttributePosition,
      shaderDCAttributeColors,
      shaderDTProgram,
      shaderDTUniformModel,
      shaderDTUniformView,
      shaderDTUniformProjection,
      shaderDTUniformTexture,
      shaderDTAttributePosition,
      shaderDTAttributeTexCoord )

import qualified Graphics.SDLGles.Types as SDG
                 ( Config )

import           Graphics.SGCDemo.Config
                 ( doDebug
                 , isEmbedded
                 , useGLES
                 , mvpConfig
                 , wolfAmbientStrength
                 , wolfSpecularStrength )

import           Graphics.SGCDemo.ConfigInline
                 ( configInline )

import           Graphics.SGCDemo.CubeFaces ( drawCubeFaces )

import           Graphics.SGCDemo.Wolf
                 ( bodyPng64
                 , furPng64
                 , wolfMtl
                 , eyesPng64 )

import           Graphics.SGCDemo.WolfObj ( wolfObj )

import qualified Graphics.SGCDemo.Scene as GsgcScene ( sceneObj, sceneMtl )

import           Graphics.SGCDemo.Types
                 ( App'
                 , Config
                 , ConfigWolfFrames (ConfigWolfFramesStr, ConfigWolfFramesNum)
                 , Bubble (Bubble)
                 , Shader' (Shader'C, Shader'T)
                 , Flipper (NotFlipping, Flipping)
                 , FlipDirection (FlipAscending, FlipDescending)
                 , FlipHemisphere (FlipUpper, FlipLower)
                 , SGCAppUserData (SGCAppUserData)
                 , Buffer (Buffer)
                 , BufferMaker (MakeBufferScalar, MakeBufferVertex3, MakeBufferVertex4)
                 , MainLoop (MainLoop)
                 , matrixVarsModel
                 , matrixVarsView
                 , matrixVarsProjection
                 , mainLoopConfig
                 , mainLoopApp'
                 , mainLoopShaders
                 , mainLoopBufferMb
                 , mainLoopTexMaps
                 , mainLoopMeshes
                 , mainLoopFlipper
                 , mainLoopT
                 , mainLoopRands
                 , mainLoopCameraZ
                 , mainLoopArgs
                 , shaderVarsCAp
                 , shaderVarsCAc
                 , shaderVarsCAn
                 , shaderVarsTAp
                 , shaderVarsTAtc
                 , shaderVarsTAn
                 , shaderVarsTUt
                 , shaderVarsTUtim
                 , shaderVarsTUdvo
                 , shaderVarsTUdofog
                 , shaderVarsMAp
                 , shaderVarsMAtc
                 , shaderVarsMAn
                 , shaderVarsMUse
                 , shaderVarsMUac
                 , shaderVarsMUdc
                 , shaderVarsMUsc
                 , shaderVarsMUt
                 , shaderVarsMUas
                 , shaderVarsMUss
                 , shaderVarsMUtim
                 , shaderVarsMSAp
                 , shaderVarsMSAtc
                 , shaderVarsMSAn
                 , shaderVarsMSUse
                 , shaderVarsMSUdc
                 , shaderVarsMSUsc
                 , shaderVarsMSUt
                 , bufferLength
                 , bufferAry
                 , app'Config
                 , sgcAppConfig
                 , configViewportWidth
                 , configViewportHeight
                 , configFaceSpec
                 , configDoWolf
                 , configDoScene
                 , configSceneScale
                 , configDoInitRotate
                 , configDoCube
                 , configCubeFacesInner
                 , configCubeFacesOuter
                 , configDoCarrousel
                 , configDoTorus
                 , configDoBackground
                 , configDoTransformTest
                 , configDoStars
                 , configDoHoop
                 , configHoopVelocityX
                 , configHoopVelocityY
                 , configHoopColor
                 , configWolfFrames
                 , isFlipping
                 , shaderMatrix
                 , shaderProgram
                 , shaderShaderVars
                 , bubbleXVelocity
                 , bubbleYVelocity
                 , bubbleXPos
                 , bubbleYPos
                 , bubbleRadius
                 , bubbleColor
                 , bubbleLineWidth
                 , flHemisphere
                 , flDirection
                 , flAngleDeg
                 , flAngleDegPrev
                 , flTick
                 )
import           Graphics.SDLGles.SDL.Events ( processEvents )
import           Graphics.SDLGles.App ( init, runLoop )

type Loggers = (Logger, Logger, Logger)

-- gnuplot> plot [x=0:20] [0:180] x ** 1.75
-- gnuplot> plot [x=0:20] [0:180] (x / 5) ** 4
-- gnuplot> plot [x=0:20] [0:180] 1.5 ** x - 1.5
-- gnuplot> plot [x=0:20] [0:180] 1.3 ** x - 1.5
-- flipTickToAng = min 180 . (^^ 14) . (/ 20.0) . fromIntegral
-- flipTickToAng = min 180 . (** (1.75 :: Float)) . fromIntegral
-- flipTickToAng = min 180 . (** 4) . (/ 5) . fromIntegral
flipTickToAng x = min 180 . (+ inv 1.5) $ (1.3 ** fromIntegral x)
numTicksPerFlip = 20

coneSectionSpinPeriod = 40
coneSectionSpinFactor = 120

-- 1 = static background
-- 0 = no parallax
backgroundParallax = 0.8

mouseWheelFactor = 1 / 4.0
maxTranslateZ = inv 2.5

blendFunc'                    = ( SrcAlpha, OneMinusSrcAlpha )
blendFuncPolygonAntialiasing' = ( SrcAlphaSaturate, One )
shadeModel'                   = Smooth

sphereColor = color 0 57 73 255

faceSpec app config rands = do
    let log = appLog app
        sdlGlesConfig = appConfig app
    info log "Please wait . . . (parsing face specs, this will take a while)"
    catFrames <- cycle <$> SCC.renderFrames False

    let decode = decodeImage' log

    img1' <- decode imageNefeli
    img2' <- decode imageNeske
    img3' <- decode imageWolf
    imgViking' <- decode imageViking
    imgHarlem' <- decode imageHarlem
    imgShigeru' <- decode imageShigeru
    imgFence' <- decode imageFence
    imgLondon' <- decode imageLondon
    img5' <- decode imagePlaza1
    img6' <- decode imagePlaza2
    img7' <- decode imagePlaza3
    img8' <- decode imagePlaza4
    imgGalerie4' <- decode imageGalerie4
    imgGalerie5' <- decode imageGalerie5
    imgGalerie6' <- decode imageGalerie6
    imgGalerie7' <- decode imageGalerie7
    imgGalerie8' <- decode imageGalerie8
    imgGalerie9' <- decode imageGalerie9
    imgGalerie10' <- decode imageGalerie10

    imtTest' <- decode imageWolf

    let g1 = GraphicsSingle img1' True
        g2 = GraphicsSingle img2' True

    gmovieCaspian' <- mapM decode movieCaspian
    gmovieBat' <- mapM decode movieBat

    let (withCairo, noCairo) = (textureWithCairo sdlGlesConfig, textureNoCairo sdlGlesConfig)

    let gmovieCaspian = GraphicsMoving (gmovieCaspian' `deepseq` cycle gmovieCaspian') 1 0
        gmovieBat = GraphicsMoving (gmovieBat' `deepseq` cycle gmovieBat') 1 0
        gcat = GraphicsSingleCairo Nothing catFrames
        g5 = GraphicsSingleCairo (Just img2') (bubbleFrames rands 0)
        g6 = GraphicsSingleCairo Nothing (bubbleFrames rands 0)
        gwolf = GraphicsSingle img3' True
        gviking = GraphicsSingle imgViking' True
        gharlem = GraphicsSingle imgHarlem' True
        gshigeru = GraphicsSingle imgShigeru' True
        glondon = GraphicsSingle imgLondon' True
        gfence = GraphicsSingle imgFence' True
        gplaza1 = GraphicsSingle img5' True
        gplaza2 = GraphicsSingle img6' True
        gplaza3 = GraphicsSingle img7' True
        gplaza4 = GraphicsSingle img8' True
        ggalerie4 = GraphicsSingle imgGalerie4' True
        ggalerie5 = GraphicsSingle imgGalerie5' True
        ggalerie6 = GraphicsSingle imgGalerie6' True
        ggalerie7 = GraphicsSingle imgGalerie7' True
        ggalerie8 = GraphicsSingle imgGalerie8' True
        ggalerie9 = GraphicsSingle imgGalerie9' True
        ggalerie10 = GraphicsSingle imgGalerie10' True

    -- | these calls allocate the backing pixel arrays.
    -- | it seems that it is ok to only allocate one of each kind and reuse
    --   it.

    tnc_512_1 <- noCairo 512 512

    tnc_256_1 <- noCairo 256 256

    twc_512_1 <- withCairo 512 512

    let tcat = twc_512_1
        tharlem = tnc_256_1
        tshigeru = tnc_256_1
        tfence = tnc_256_1
        tlondon = tnc_256_1
        t1 = tnc_512_1
        t2 = tnc_512_1
        tmovie = tnc_512_1
        tgalerie0 = tnc_256_1
        tgalerie1 = tnc_256_1
        tgalerie2 = tnc_256_1
        tgalerie3 = tnc_256_1
        tgalerie4 = tnc_256_1
        tgalerie5 = tnc_256_1
        tgalerie6 = tnc_256_1
        tgalerie7 = tnc_256_1
        tgalerie8 = tnc_256_1
        tgalerie9 = tnc_256_1
        tgalerie10 = tnc_256_1
    twolf <- noCairo 512 256

    -- outer / inner
    let faceSpecExtreme      = [ (tcat, gcat), (t2, g2), (tmovie, gmovieCaspian), (t1, g1)
                               , (tnc_256_1, gviking), (tnc_256_1, gplaza2), (tcat, gcat), (twc_512_1, g5) ]

    let faceSpecSimple       = [ (t1, g1), (t1, g1), (t1, g1), (t1, g1)
                               , (t2, g2), (t2, g2), (t2, g2), (t2, g2) ]

    let faceSpecOneMovie     = [ (t1, g1), (t1, g1), (t1, g1), (t1, g1)
                               , (t2, g2), (t2, g2), (t2, g2), (tmovie, gmovieCaspian) ]

    let faceSpecNoMovie      = [ (t1, g1), (t1, g1), (t1, g1), (t1, g1)
                               , (t2, g2), (t2, g2), (tcat, gcat), (twc_512_1, g5) ]

    let faceSpecEightMovies  = [ (tmovie, gmovieCaspian), (tmovie, gmovieCaspian), (tmovie, gmovieCaspian), (tmovie, gmovieCaspian)
                               , (tmovie, gmovieCaspian), (tmovie, gmovieCaspian), (tmovie, gmovieCaspian), (tmovie, gmovieCaspian) ]

    let faceSpecOneCat       = [ (tcat, gcat), (t1, g1), (t1, g1), (t1, g1)
                               , (t1, g1), (t1, g1), (t1, g1), (t1, g1) ]

    let faceSpecTwoCats      = [ (tcat, gcat), (tcat, gcat), (t1, g1), (t1, g1)
                               , (tnc_256_1, gviking), (tnc_256_1, gplaza2), (tshigeru, gshigeru), (tlondon, glondon) ]

    let faceSpecFourCats     = [ (tcat, gcat), (tcat, gcat), (tcat, gcat), (tcat, gcat)
                               , (t1, g1), (t1, g1), (t1, g1), (t1, g1) ]

    let faceSpecEightCats    = [ (tcat, gcat), (tcat, gcat), (tcat, gcat), (tcat, gcat)
                               , (tcat, gcat), (tcat, gcat), (tcat, gcat), (tcat, gcat) ]

    let faceSpecEightBubbles = [ (twc_512_1, g6), (twc_512_1, g6), (twc_512_1, g6), (twc_512_1, g6)
                               , (twc_512_1, g6), (twc_512_1, g6), (twc_512_1, g6), (twc_512_1, g6) ]

    let faceSpecViking       = [ (tnc_256_1, gviking), (tnc_256_1, gviking), (tnc_256_1, gviking), (tnc_256_1, gviking)
                               , (tnc_256_1, gplaza1), (tnc_256_1, gplaza3), (tnc_256_1, gplaza4), (tnc_256_1, gplaza2) ]

    let faceSpecEightPics    = [ (tharlem, gharlem), (tshigeru, gshigeru), (tfence, gfence), (tlondon, glondon)
                               , (tnc_256_1, gplaza1), (tnc_256_1, gplaza3), (tnc_256_1, gplaza4), (tnc_256_1, gplaza2) ]

    let faceSpecGalerie      = [ (tmovie, gmovieCaspian), (tgalerie5, ggalerie7), (tshigeru, gshigeru), (tgalerie7, ggalerie9)
                               , (tgalerie0, ggalerie10), (tgalerie1, ggalerie5), (tgalerie3, ggalerie6), (tgalerie4, ggalerie4) ]

    let faceSpecTwoMovies    = [ (tharlem, gharlem), (tshigeru, gshigeru), (tfence, gfence), (tlondon, glondon)
                               -- it *is* ok to reuse the tmovie texture
                               , (tnc_256_1, gplaza1), (tnc_256_1, gplaza3), (tmovie, gmovieCaspian), (tmovie, gmovieBat) ]

    let faceSpec' = config & configFaceSpec

    let spec | faceSpec' == "extreme"       =  pure faceSpecExtreme
             | faceSpec' == "simple"        =  pure faceSpecSimple
             | faceSpec' == "onecat"        =  pure faceSpecOneCat
             | faceSpec' == "twocats"       =  pure faceSpecTwoCats
             | faceSpec' == "fourcats"      =  pure faceSpecFourCats
             | faceSpec' == "eightcats"     =  pure faceSpecEightCats
             | faceSpec' == "eightbubbles"  =  pure faceSpecEightBubbles
             | faceSpec' == "onemovie"      =  pure faceSpecOneMovie
             | faceSpec' == "twomovies"     =  pure faceSpecTwoMovies
             | faceSpec' == "nomovie"       =  pure faceSpecNoMovie
             | faceSpec' == "eightmovies"   =  pure faceSpecEightMovies
             | faceSpec' == "vikingplaza"   =  pure faceSpecViking
             | faceSpec' == "eightpics"     =  pure faceSpecEightPics
             | faceSpec' == "galerie"       =  pure faceSpecGalerie
             | otherwise                    =  die log $ printf "Unknown value for faceSpec: %s" faceSpec'

    let toGraphTextMapping' (name, (tex, graphics)) = GraphicsTextureMapping graphics tex name
    texNames <- createTextures2DSimple app 8
    zipWith (curry toGraphTextMapping') texNames <$> spec

-- | the goal of one of these 'texture specs' is:
-- • decode the base64 image using JuicyPixels into an internal image
--   representation.
-- • assemble a GraphicsData object, based on whether it's a movie or image
--   and whether it needs Cairo, and give it the decoded image.
-- • use `textureWithCairo` or `textureNoCairo` to initialise a texture.
-- • request a new TextureObject from GL.
-- • put them together in a GraphicsTextureMapping structure.
-- • remember to call `updateTextures` at least once to actually copy the
--   pixels, and multiple times if it's a movie.

headFailM :: MonadFail m => [Char] -> m [a] -> m a
headFailM msg f = f >>= \case
    [] -> fail msg
    x : _ -> pure x

headFail :: MonadFail m => [Char] -> [a] -> m a
headFail msg = \case
    [] -> fail msg
    x : _ -> pure x

backgroundTextureSpec app config = do
    let log = appLog app
        sdlGlesConfig = appConfig app
        decode = decodeImage' log
    movie' <- mapM decode movieBat
    let graphics' = GraphicsMoving (movie' `deepseq` cycle movie') 1 0
    let (withCairo, noCairo) = (textureWithCairo sdlGlesConfig, textureNoCairo sdlGlesConfig)
    tex' <- noCairo 512 512
    let x = createTextures2DSimple app 1
    texName' <- headFailM "backgroundTextureSpec: texture empty" $ createTextures2DSimple app 1
    pure $ GraphicsTextureMapping graphics' tex' texName'

meshSpec' :: Log -> SDG.Config -> Cmog.TextureMap -> IO [(Tex, GraphicsData)]
meshSpec' log sdlGlesConfig textureMap = meshSpecGraphicsDecode log sdlGlesConfig textures' where
   tex' config' = (Cmog.tcWidth config', Cmog.tcHeight config', Cmog.tcImageBase64 config')
   textures' = map tex' textureConfigs'
   textureConfigs' = values textureMap

meshSpec app sdlGlesConfig textureMap = spec' >>= mapM map' where
    log = appLog app
    spec' = meshSpec' log sdlGlesConfig textureMap
    map' (tex, graphics) = GraphicsTextureMapping graphics tex <$> newTexture
    newTexture = headFailM "meshSpec: empty texture" $ createTextures2DSimple app 1

meshSpecGraphicsDecode :: Log -> SDG.Config -> [(Int, Int, ByteString)] -> IO [(Tex, GraphicsData)]
meshSpecGraphicsDecode log sdlGlesConfig spec = do
    let w   =  fst3
        h   =  snd3
        img =  thd3
        gs' :: IO [GraphicsData]
        gs' =  forM spec  $ \spec' -> do
            img' <- decodeImage' log . img $ spec'
            pure $ GraphicsSingle img' True
    ts      <- forM spec $ \spec' -> textureNoCairo sdlGlesConfig (w spec') (h spec')
    gs <- gs'
    pure $ zip ts gs

dimension = 0.5

launch :: Loggers -> Maybe (Int, Int) -> IO ()
launch x y = launch' x y []

launchWithArgs :: Loggers -> Maybe (Int, Int) -> [String] -> IO ()
launchWithArgs = launch'

launch' :: Loggers -> Maybe (Int, Int) -> [String] -> IO ()
launch' loggers@(info', warn', error') dimsMb args = do
    let log = Log info' warn' error'
        debug' = debug log

    configYaml <- if isEmbedded then pure configInline
                                else readFileBS loggers "forgot to copy config.yaml.example?" "config.yaml"

    config <- do  let  error' err = die log $ "Couldn't decode config.yaml: " ++ show err
                  either error' pure $ Y.decodeEither' configYaml

    let doWolf        = config & configDoWolf
        doScene       = config & configDoScene

    let dims' Nothing = do  info' $ "Getting width & height from config"
                            pure (configViewportWidth config, configViewportHeight config)
        dims' (Just (width', height')) = do
                            info' $ "Got width and height from caller"
                            pure (width', height')
    (viewportWidth, viewportHeight) <- dims' dimsMb
    info' $ printf "Viewport dims: %s x %s" (show viewportWidth) (show viewportHeight)

    let sdlGlesConfig = defaultConfig { windowWidth = viewportWidth
                                      , windowHeight = viewportHeight }

    app <- init loggers sdlGlesConfig (SGCAppUserData config) mvpConfig args

    (wolfTextureSpec', wolfSeqMb') <- do
        textureConfigYaml' <- Just <$> wolfTextureConfigYaml bodyPng64 eyesPng64 furPng64
        let mtlObjSpec' = pure (wolfObj, wolfMtl)
        if not doWolf then pure $ ([], Nothing)
                      else initMeshes app textureConfigYaml' mtlObjSpec' =<< (Just <$> getMeshLimitFrames app)

    (_, sceneSeqMb') <- do
        -- @mock using the wolf's fur as the texture config
        let textureConfigYaml' = Just $ sceneTextureConfigYaml furPng64
            -- xxx will not work on mobile
            mtlObjSpec' = sequence2 (GsgcScene.sceneObj, GsgcScene.sceneMtl)
        if not doScene then pure $ ([], Nothing)
                       else initMeshes app textureConfigYaml' mtlObjSpec' =<< (Just <$> getMeshLimitFrames app)

    let getSceneBuffers' = do
          let Cmog.Sequence frames = fromJust sceneSeqMb'
          frame <- headFail "xxx: no (more) frames" frames
          let Cmog.SequenceFrame bursts = frame
          initBuffers bursts [ MakeBufferVertex3 -- pos
                             , MakeBufferVertex4 -- tc
                             , MakeBufferVertex4 -- normals
                             ]
    sceneBuffersMb' <- if doScene then Just <$> getSceneBuffers' else pure Nothing

    let doInitRotate = config & configDoInitRotate
        rotations | doInitRotate     = multMatrices [ rotateX 15, rotateY $ inv 15 ]
                  | otherwise        = identityMatrix

    let app' = app & appMultiplyModel rotations

    rands <- randoms

    textureMappingCube <- faceSpec app config rands
    let textureMappingWolf = wolfTextureSpec'
    textureMappingBg <- backgroundTextureSpec app config

    debug' "initShaders"
    (colorShader, texShader, meshShader, meshShaderScene) <- initShaders log

    debug' "starting loop"

    let meshes = ( Cmog.makeInfiniteSequence <$> wolfSeqMb'
                 , Cmog.makeInfiniteSequence <$> sceneSeqMb' )

    runLoop app appLoop $ MainLoop
        config
        app'
        (colorShader, texShader, meshShader, meshShaderScene)
        sceneBuffersMb' (textureMappingCube, textureMappingWolf, textureMappingBg)
        meshes (NotFlipping FlipUpper)
        0 -- cameraZ
        0 -- t
        rands args

appLoop curLoop = do
    let log = appLog app
        info' = info log
        debug' = debug log
        dim = dimension

        (wolfSeqMb', sceneSeqMb') = meshes

        doScene    = config  & configDoScene
        sceneScale = config  & configSceneScale

        appConfig' = appConfig app

        config     = curLoop & mainLoopConfig
        app        = curLoop & mainLoopApp'
        shaders    = curLoop & mainLoopShaders
        buffersMb  = curLoop & mainLoopBufferMb
        meshes     = curLoop & mainLoopMeshes
        flipper    = curLoop & mainLoopFlipper
        cameraZ    = curLoop & mainLoopCameraZ
        t          = curLoop & mainLoopT
        rands      = curLoop & mainLoopRands
        args       = curLoop & mainLoopArgs

        (textureMappingCube, textureMappingWolf, textureMappingBg)
                   = curLoop & mainLoopTexMaps

    debug' "* looping"

    debug' "updating textures"
    textureMappingsCube' <- updateTextures app textureMappingCube
    -- GraphicsSingle, so technically not necessary to do it on every
    -- iteration, but has to happen at least once.
    textureMappingWolf' <- updateTextures app textureMappingWolf
    textureMappingBg'   <- headFailM "appLoop: missing texture" $ updateTextures app [textureMappingBg]
    debug' "done updating textures"

    let viewportWidth = windowWidth appConfig'
        viewportHeight = windowHeight appConfig'
    (qPressed, click, clickRelease, clickPress, dragAmounts, wheelOrPinchAmount) <- processEvents log (viewportWidth, viewportHeight)

    debug' $ printf "wheel or pinch: %s" (show wheelOrPinchAmount)
    debug' $ printf "dragAmounts: %s" (show dragAmounts)
    when (isJust click) . debug' $ printf "click: %s" (show click)

    let remainingTranslateZ' = getRemainingTranslateZ app

    (app', cameraZDelta') <- pure $ if (isJust wheelOrPinchAmount)
        -- mouse/touch event
        then do  let view' = translateZ dz''
                     dz' = (* mouseWheelFactor) . frint . fromJust $ wheelOrPinchAmount
                     dz'' = min dz' remainingTranslateZ'
                 (app & appMultiplyView view', dz'')
        -- no mouse/touch event
        else (app, 0)

    let cameraZNew' = cameraZ + cameraZDelta'

    wrapGL log "clear" $ GL.clear [ ColorBuffer, DepthBuffer ]

    let ( colorShader, texShader, meshShader, meshShaderScene ) = shaders
        doBackground = config & configDoBackground

    when doBackground $ do
        let tx = -0.5 * dim; ty = tx; tz = 2 * tx
            model' = multMatrices [ translateX tx
                                  , translateY ty
                                  , translateZ tz
                                  , scaleX 15
                                  , scaleY 15 ]
        let app'' = app' & appReplaceModel identityMatrix
                         & appMultiplyModel model'
        drawBackgroundTexture app'' texShader textureMappingBg' cameraZNew' args

    let rotationsMouse' = rotationsForDrag dragAmounts
        rotateForFlipper (NotFlipping hemi) = 0
        rotateForFlipper _ = flAngleDeg flipper - flAngleDegPrev flipper
        rotationFlipper' = rotateZ . rotateForFlipper $ flipper

    app'' <- pure $ app' & appMultiplyRightModel rotationsMouse'
                         & appMultiplyModel rotationFlipper'

    when (config & configDoStars)     $ drawStars app'' (colorShader, texShader) t args
    when (config & configDoCarrousel) $ carrousel app'' (colorShader, texShader) textureMappingsCube' t args

    -- must be before the cube.
    when (config & configDoWolf)      $ do
        let wolfSeq = fromJust wolfSeqMb'
        drawWolf app'' wolfSeq meshShader textureMappingWolf' flipper t args
    when doScene $ do
        let sceneSeq = fromJust sceneSeqMb'
            sceneBuf = fromJust buffersMb
        drawScene app'' sceneSeq sceneScale meshShaderScene sceneBuf flipper t args

    when (config & configDoHoop) $
        drawHoop app'' config colorShader t args

    hit <- ifM' False (config & configDoCube) do
        let inner = config & configCubeFacesInner
            outer = config & configCubeFacesOuter
        _app <- cube app'' (colorShader, texShader) textureMappingsCube' inner outer t flipper args
        checkVertexHit _app click
    when (config & configDoTorus) $ drawTorus app'' (colorShader, texShader) textureMappingsCube' t args

    rands' <- case rands of
                [] -> fail "no more rands"
                _ : xs -> pure xs
    let newLoop = curLoop { mainLoopT       = t + 1
                          , mainLoopFlipper = updateFlipper flipper hit
                          , mainLoopApp'    = app''
                          , mainLoopMeshes  = ( Cmog.tailSequence <$> wolfSeqMb'
                                              , Cmog.tailSequence <$> sceneSeqMb' )
                          , mainLoopRands   = rands'
                          , mainLoopCameraZ = cameraZNew'
                          , mainLoopTexMaps = (textureMappingsCube', textureMappingWolf', textureMappingBg') }

        continue | qPressed = Nothing
                 | otherwise = Just newLoop

    pure continue

cube :: App' -> (Shader', Shader') -> [GraphicsTextureMapping] ->
        [Int] -> [Int] -> Int -> Flipper -> [String] -> IO App'

cube app shaders texMaps facesInner facesOuter t flipper args = do
    let log          = appLog app
        appmatrix    = appMatrix app
        debug'       = debug log
        isFlipping'  = isFlipping flipper
        dim          = dimension
        shader       = fst shaders
        prog         = shaderProgram shader
        matrixVars   = shaderMatrix shader
        umm          = matrixVars & uniformUniformLocation . matrixVarsModel
        umv          = matrixVars & uniformUniformLocation . matrixVarsView
        ump          = matrixVars & uniformUniformLocation . matrixVarsProjection
        shaderVars   = shaderShaderVars shader
        ap           = shaderVars & attribAttribLocation . shaderVarsCAp
        ac           = shaderVars & attribAttribLocation . shaderVarsCAc
        an           = shaderVars & attribAttribLocation . shaderVarsCAn
        shader'      = ShaderDC (Just prog) umm umv ump ap ac an
        tos = map graphicsTextureMappingTextureObject texMaps
        scale' = mvpConfigCubeScale mvpConfig
        model' = multMatrices [ scaleY scale'
                              , scaleX scale'
                              , scaleZ scale' ]
        app' = app & appMultiplyModel model'

    debug' "drawing cube"
    drawCubeFaces app' shaders dimension facesInner facesOuter flipper t tos args

    debug' "drawing sphere"
    unless isFlipping' $ drawSphere app' shader' dim

    pure app'

drawSphere app shader dim = sphere' where
    sphere' = sphere app' shader (slices', stacks') col r
    app'    = app & appMultiplyModel model'
    model'  = multMatrices [ translateZ dim
                           , translateY dim
                           , translateX dim ]
    col     = sphereColor
    r       = 0.025
    slices' = 10
    stacks' = slices'

debug | doDebug   = info
      | otherwise = const . const . pure $ ()

toRight (Right x) = x
toRight _         = error "not Right"
toLeft (Left x)   = x
toLeft _          = error "not Left"

-- (arbitrary choice of whether x is first or y, but these are small amounts
-- so it shouldn't matter too much)
rotationsForDrag (Just (x, y)) = do
    let rx' = frint y
        ry' = frint x
    multMatrices [ rotateX rx', rotateY ry' ]
rotationsForDrag Nothing       = identityMatrix

drawBackgroundTexture app shader mapping cameraZ args = do
    let log           = appLog app
        appmatrix     = appMatrix app
        prog          = shaderProgram shader
        shaderVars    = shaderShaderVars shader
        shaderMatrix' = shaderMatrix shader
        utt           = shaderVars & uniformUniformLocation . shaderVarsTUt
        udfog         = shaderVars & uniformUniformLocation . shaderVarsTUdofog
        shaderT       = toShaderDT Nothing shaderMatrix' shaderVars
        zAdjust'      = inv cameraZ * backgroundParallax
        model'        = multMatrices [ translateZ zAdjust' ]
        app'          = app & appMultiplyModel model'
    -- 3rd 0, 4th 1
    let tc00 = ver4 0 0 0 1
        tc01 = ver4 0 1 0 1
        tc10 = ver4 1 0 0 1
        tc11 = ver4 1 1 0 1
        GraphicsTextureMapping _ _ texName' = mapping
    -- use the program here and set shaderT's program to Nothing.
    useShader log prog
    uniform log "do fog" udfog $ float 0
    rectangleTex app' shaderT dimension dimension texName' (tc00, tc01, tc10, tc11)
    pure ()

drawBackgroundColor app shader args = do
    let log                 = appLog app
        appmatrix           = appMatrix app
        prog                = shaderProgram shader
        matrixVars          = shaderMatrix shader
        shaderVars          = shaderShaderVars shader
        umm                 = matrixVars & uniformUniformLocation . matrixVarsModel
        umv                 = matrixVars & uniformUniformLocation . matrixVarsView
        ump                 = matrixVars & uniformUniformLocation . matrixVarsProjection
        ap                  = shaderVars & attribAttribLocation . shaderVarsCAp
        ac                  = shaderVars & attribAttribLocation . shaderVarsCAc
        an                  = shaderVars & attribAttribLocation . shaderVarsCAn
        (model, view, proj) = map3 stackPop' appmatrix

    useShader log prog

    uniform log "model" umm =<< toMGC model
    uniform log "view"  umv =<< toMGC view
    uniform log "proj"  ump =<< toMGC proj

    rectangle app (ap, ac, an) (c1, c2, c3, c4) (v1, v2, v3, v4) where
        a  = 60
        z  = inv 2
        dim'  = 5
        c1 = color 100 10 10 a
        c2 = color 90 10 10 a
        c3 = color 80 10 10 a
        c4 = color 70 10 10 a
        v1 = verple3 (inv dim', inv dim', z)
        v2 = verple3 (inv dim',     dim', z)
        v3 = verple3 (    dim',     dim', z)
        v4 = verple3 (    dim', inv dim', z)

updateFlipper flipper hit = flip' flipper where
    initFlip' FlipUpper = FlipDescending
    initFlip' FlipLower = FlipAscending
    flip' flipper'@(NotFlipping hemi)
      | hit == False = flipper'
      | otherwise = let angleDeg = if hemi == FlipUpper then 0 else 180 in
                    Flipping { flHemisphere = hemi
                             , flDirection = initFlip' hemi
                             , flAngleDeg     = angleDeg
                             , flAngleDegPrev = angleDeg
                             , flTick = 0 }
    flip' flipper'@(Flipping hemi dir ang _ tick)
      | hit = flipper'
      | tick == end' = NotFlipping hemi
      | otherwise = flipper' { flHemisphere = if ang < 90 then FlipUpper else FlipLower
                            , flAngleDeg = toAng' dir tick
                            , flAngleDegPrev = ang
                            , flTick = tick + 1 }
    end' = numTicksPerFlip
    toAng' dir tick = flipTickToAng $ u' dir tick
    u' FlipDescending tick = tick
    u' FlipAscending  tick = numTicksPerFlip - tick

checkVertexHit app click | isJust click = checkVertexHit' app click
                         | otherwise = pure False

-- reading the depth buffer is not reliable (doesn't work on android and
-- supposedly causes a big performance hit), so unprojecting the mouse click
-- to look for the vertex becomes more difficult.
-- we do it by testing at a number of different depths (currently 10).
-- seems to work.
-- downside: if the vertex is behind other fragments it can still be clicked
-- on.
-- doesn't need to be impure, but easy to show debug statements.

checkVertexHit' app click = do
    let log = appLog app
        debug'      = debug log
        depthMax = 1.0
        depthMin = 0.1
        check' = map (checkVertexHit'' app click) [0, depthMin .. depthMax]
        find' = findIndex id check'

    -- not reliable.
    -- depth' <- getClickDepth log click

    let ret' Nothing = do
            debug' "No hits"
            pure False
        ret' (Just x) = do
            let y = frint x * 0.1 :: Float
            debug' $ printf "Got a hit at %.1f" y
            pure True
    ret' find'

checkVertexHit'' app click depth' = maybe invalid' valid' clickLocation where
    log = appLog app
    appConfig' = appConfig app
    config = app'Config app
    viewportWidth = windowWidth appConfig'
    viewportHeight = windowHeight appConfig'
    dim = dimension
    Just (x, y, _) = click
    x' = frint x
    y' = frint y

    clickLocation = unProjectItF app (viewportWidth, viewportHeight) $ Vertex3 x' y' depth'

    -- debug' "uncounterintracontravertible"
    invalid' = False

    valid' (Vertex3 xup yup zup) = isClickHit (xup, yup, zup) (dim, dim, dim)

    -- debug' $ printf "unproject (%d, %d, %.1f) => %.1f %.1f %.1f" x y depth' xup yup zup

isClickHit (vert1X, vert1Y, vert1Z) (targetX, targetY, targetZ) = vertXHit && vertYHit && vertZHit where
    closeTo l m = (< 0.1) (abs $ l - m)
    vertXHit = vert1X `closeTo` targetX
    vertYHit = vert1Y `closeTo` targetY
    vertZHit = vert1Z `closeTo` targetZ

a `between` (b, c) = a >= l && a <= r where
    (l, r) | b <= c       = (b, c)
           | otherwise    = (c, b)

decodeImage imageBase64 = do
    let imageBase64Decoded = B64.decode imageBase64
    (when . isLeft $ imageBase64Decoded) $
        err' . toLeft $ imageBase64Decoded
    let imageBase64Decoded' = toRight imageBase64Decoded
        dynamicImage = decodePng imageBase64Decoded'

    (when . isLeft $ dynamicImage) $
        err'' . toLeft $ dynamicImage
    dynamicImage where
        err' e = Left $ "Couldn't decode base64, " ++ e
        err'' e = Left $ "Couldn't decode png, " ++ e

decodeImage' log imageBase64 =
    let img = decodeImage imageBase64 in
    if isLeft img then die log "Bad image"
                  else pure $ toRight img

bubbleFrames :: [Double] -> Int -> [C.Render ()]
bubbleFrames rands t = bubbleFrames' rands t []

bubbleFrames' rands t [] = bubbleFrames' rands t bubbles' where
    bubbles' = [ Just $ Bubble 1 10 0 0 20 (1, 0, 1, 1) 5
               , Just $ Bubble 2 (inv 10) 20 0  5 (0, 1, 1, 1) 10
               , Just $ Bubble (inv 2) 10 300 200 15 (0, 0, 1, 1) 1
               , Just $ Bubble (inv 3) 10 300 200 25 (0, 0, 1, 1) 5
               , Just $ Bubble 2 10 300   0 10 (0, 0, 1, 1) 5 ]

bubbleFrames' rands t bubbles = frame : bubbleFrames' (drop 1 rands) (t + 1) bubbles' where
    t' = frint t
    bubbles' = map (advanceBubble rands) bubbles
    rgba' (a, b, c, d) = C.setSourceRGBA a b c d
    drawBubble' bubble = do
        let x = bubbleXPos bubble
            y = bubbleYPos bubble
            r = bubbleRadius bubble
            c = bubbleColor bubble
            th = bubbleLineWidth bubble
        C.save
        C.translate x y
        C.moveTo r 0
        C.arc 0 0 r 0 (2 * pi)
        C.setLineWidth th
        rgba' c
        C.stroke
        C.restore
    frame = do
        let doBubble' Nothing = pure ()
            doBubble' (Just bubble) = drawBubble' bubble
        C.translate 512 512
        C.rotate pi
        mapM_ doBubble' bubbles

advanceBubble rands Nothing = Nothing
advanceBubble rands (Just bubble) = do
    let xv = bubbleXVelocity bubble
        yv = bubbleYVelocity bubble
        x  = bubbleXPos bubble
        y  = bubbleYPos bubble
        x' = x + xv
        y' = y + yv
        yv' = yv * yv''
        yv'' | r1 < 0.3 = 0.8
             | r1 > 0.6 = 1.2
             | otherwise = 1
        xv' = xv + xv''
        xv'' = 0
        r1 = (!! 0) rands
        bubble' | x < 0 || x > 512 || y < 0 || y > 512 = Nothing
                | otherwise = pure $ bubble { bubbleXPos = x'
                                            , bubbleYPos = y'
                                            , bubbleXVelocity = xv'
                                            , bubbleYVelocity = yv' }
    bubble'

-- | not possible with GLES.
-- • also, supposedly really bad for performance: forces pipeline flush.

-- getClickDepth log click = do
    -- let info' = info log
        -- debug' = debug log
    -- ptr' <- mallocArray 1 :: IO (Ptr GLfloat)
    -- let pos' = Position x' y'
        -- x' = fst . fromJust $ click
        -- y' = frint viewportHeight - yy'
        -- yy' = snd . fromJust $ click
        -- size' = Size 1 1
        -- pd' = PixelData GL.DepthComponent GL.Float ptr'
    -- wrapGL log "readPixels" $ readPixels pos' size' pd'
    -- p <- peekElemOff ptr' 0
    -- free ptr'
    -- info' $ printf "DEPTH: %.3f" p
    -- pure p

initShaders log = do
    let mvp = ("model", "view", "projection")
        colorShader     = initShaderColor log
        texShader       = initShaderTexture log
        meshShader      = initShaderMesh log
        meshShaderScene = initShaderMeshScene log

    (,,,) <$> colorShader <*> texShader <*> meshShader <*> meshShaderScene

carrousel app shaders texMaps t args = do
    let app'    = app & appMultiplyModel model'
        model'  = multMatrices [ rotateY amount' ]
        period' = frint coneSectionSpinPeriod
        t'      = frint . (`mod` coneSectionSpinPeriod) $ t
        theta'  = t' / period' * 2 * pi
        amount' = frint coneSectionSpinFactor * sin theta'
    carrousel' app' shaders texMaps 1.3 t args

carrousel' app shaders texMaps radiusRatio t args = do
    let log        = appLog app
        doRotate   = False
        app'       = app & transform'
        model'     = multMatrices [ rotateX t'
                                  , rotateZ (t' * 1.1) ]
        t'         = 360 * frint t / 30
        shaderC'   = fst shaders
        shaderT'   = snd shaders

        progC' = shaderProgram shaderC'
        shaderC    = toShaderDC
                     (Just progC') (shaderMatrix shaderC') (shaderShaderVars shaderC')
        shaderT    = toShaderDT
                     Nothing       (shaderMatrix shaderT') (shaderShaderVars shaderT')
        progT      = shaderProgram shaderT'
        shaderVars = shaderShaderVars shaderT'
        udvot      = shaderVars & uniformUniformLocation . shaderVarsTUdvo

        transform' | doRotate  = appMultiplyModel model'
                   | otherwise = id

        npoly      = 60
        h          = 0.9
        circum     = 4 * h
        radius          = circum / 2 / pi
        texNames = map graphicsTextureMappingTextureObject texMaps
        (texName0:texName1:_) = texNames

        tx00 = Vertex4 0 0 0 1
        tx01 = Vertex4 0 1 0 1
        tx10 = Vertex4 1 0 0 1
        tx11 = Vertex4 1 1 0 1

        (r1, g1, b1) = hsvCycle 200 $ t
        (r2, g2, b2) = hsvCycle 200 $ t + 30
        (r3, g3, b3) = hsvCycle 200 $ t + 60

        col1 = color r1 g1 b1 255
        col2 = color r2 g2 b2 255
        col3 = color r3 g3 b3 255

        angles a      = [0, 2 * pi / a ..]
        anglesShift a = [pi / 2, pi / 2 + 2 * pi / a ..]
        tx'0 = (tx00, tx01, tx10, tx11)
        tx'1 = (tx00, tx01, tx10, tx11)
        tx'2 = (tx00, tx01, tx10, tx11)
        tx'3 = (tx00, tx01, tx10, tx11)

    -- color sheet.
    forM_ (zip4 [1 .. 3] [col1, col2, col3] (anglesShift 3) (drop 1 $ anglesShift 3)) $ \(_, c, a, b) -> do
        let cylinder'    = cylinder app' shaderC npoly h (radius * 0.99) a b c
            coneSection' = do  let radius' = radius * 0.99
                               coneSection app' shaderC npoly h radius' (radiusRatio * radius') a b c
        if radiusRatio == 1 then cylinder' else coneSection'

    useShader log progT
    uniform log "udvot" udvot glFalseF

    let textureSheet radius' = do
            forM_ (zip5 [1 .. 4] [tx'0, tx'1, tx'2, tx'3] (angles 4) (drop 1 $ angles 4) [texName0, texName1, texName0, texName1]) $ \(_, tx, a, b, texName) -> do
                let cylinderTex'    = cylinderTex app' shaderT npoly h radius' a b texName tx
                    coneSectionTex' = coneSectionTex app' shaderT npoly h radius' (radius' * radiusRatio) a b texName tx
                if radiusRatio == 1 then cylinderTex' else coneSectionTex'
    textureSheet radius
    textureSheet $ radius * 0.98

drawTorus app shaders texMaps t args = do
    let shaderC' = fst shaders
        prog' = shaderProgram shaderC'
        matrix' = shaderMatrix shaderC'
        shaderVars' = shaderShaderVars shaderC'
        shaderC = toShaderDC (Just prog') matrix' shaderVars'
        ncylinders = 40
        innerRadius = 0.5
        thickness = 0.1
    torus app shaderC innerRadius thickness ncylinders

torusTest' app shaderC innerRadius thickness ncylinders n = do
    let app' = app & appMultiplyModel model'
        t' = 0.5 - (frint (n + 1) / 10)
        t'' = t' * 10
        model' = multMatrices [ translateY t'
                              , translateX t' ]
    torus app' shaderC innerRadius thickness ncylinders

ifM' no pred yes | pred = yes
                 | otherwise = pure no

getRemainingTranslateZ app = max 0 $ maxTranslateZ - curTranslateZ' where
    view' = app & appMatrix & snd3 & stackPop'
    curTranslateZ' = (DMX.!) view' (4, 3) -- row, col, 1-based

-- quasiquotes don't seem to work in the cross-compiler (needs external
-- interpreter), but why did it work in the Mesh module?) xxx
wolfTextureConfigYaml :: ByteString -> ByteString -> ByteString -> IO ByteString
wolfTextureConfigYaml bodyPng eyesPng furPng = pure $
    "textures:\n" <>
    "  - materialName: Material\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: eyes\n" <>
    "    imageBase64: " <> eyesPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: fur\n" <>
    "    imageBase64: " <> bodyPng <> "\n" <>
    "    width: 512\n" <>
    "    height: 256\n"

-- the mapping must exist, but we're not even using it xxx
sceneTextureConfigYaml :: ByteString -> ByteString
sceneTextureConfigYaml furPng =
    "textures:\n" <>
    "  - materialName: None\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: light\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: patrulla\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: patrulla2\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: red_paint\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: red_paint_old\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: tire\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: van\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n" <>
    "  - materialName: wheel\n" <>
    "    imageBase64: " <> furPng <> "\n" <>
    "    width: 256\n" <>
    "    height: 256\n"

initMeshesParse :: Log -> Maybe ByteString -> IO ([Text], ByteString) -> IO (Either String (Cmog.Sequence, Cmog.TextureMap))
initMeshesParse log textureConfigYamlMb meshSpec = do
    (obj', mtl') <- meshSpec
    let objSources' = map Cmog.ConfigObjectSource obj'
    let meshConfig = Cmog.Config c1 c2 c3
        c1 = Cmog.ConfigObjectSpec objSources'
        c2 = Cmog.ConfigMtlSource mtl'
        c3 = textureConfigYamlMb
    info log "parsing"
    Cmog.parse meshConfig

initSceneParse :: Log -> Maybe ByteString -> IO (Either String (Cmog.Sequence, Cmog.TextureMap))
initSceneParse log textureConfigYamlMb = do
    -- xxx mobile
    mtlSrc' <- GsgcScene.sceneMtl
    sceneObj' <- GsgcScene.sceneObj
    let objSources' = map Cmog.ConfigObjectSource sceneObj'
        sceneConfig = Cmog.Config c1 c2 c3
        c1 = Cmog.ConfigObjectSpec objSources'
        c2 = Cmog.ConfigMtlSource mtlSrc'
        c3 = textureConfigYamlMb
    info log "parsing"
    Cmog.parse sceneConfig

drawHoop :: App' -> Config -> Shader' -> Int -> [String] -> IO ()
drawHoop app config shaderC' t args = do
    let progC'        = shaderProgram shaderC'
        shaderMatrix' = shaderMatrix shaderC'
        shaderVars'   = shaderShaderVars shaderC'
        shaderD       = toShaderDC (Just progC') shaderMatrix' shaderVars'

        t'' x         = 0.5 + sin x / 2.0

        n'            = frint t * spinVelocityY'
        m'            = frint t * spinVelocityX'

        appmatrix     = appMatrix app
        model'        = multMatrices [ rotateX $ 90 * t'' m'
                                     , rotateY $ 90 * t'' n']
        app'          = appMultiplyModel model' app

        spinVelocityX' =             config & configHoopVelocityX
        spinVelocityY' =             config & configHoopVelocityY
        colour         = colorHex' $ config & configHoopColor

        r = 0.5
        h = 0.1

        twopi = 2 * pi

    cylinder app' shaderD 60 h r 0 twopi colour
    pure ()

colorHex :: Int -> Float -> Vertex4 Float
colorHex hex = Vertex4 r g b where
    r = h' 16
    g = h' 8
    b = h' 0
    h' n = (/255.0) . frint $ hex `shiftR`  n .&. 255

colorHex' = colorHex'' . splitAt 6 where
    colorHex'' (col, opa) = colorHex (a col) (b opa)
    a      =                    toHex'
    b      = (/255.0) . frint . toHex'
    toHex' = fst . head' . readHex
    head' [] = error "colorHex': no read"
    head' (x : _) = x

drawScene app sceneSeq sceneScale shader buffers flipper t args = do
    let log = appLog app
        prog       = shaderProgram shader
        matrixVars = shaderMatrix shader
        umm        = matrixVars & uniformUniformLocation . matrixVarsModel
        umv        = matrixVars & uniformUniformLocation . matrixVarsView
        ump        = matrixVars & uniformUniformLocation . matrixVarsProjection
        Cmog.Sequence frames' = sceneSeq

    -- infinite list => head is fine.
    head' <- headFail "drawScene: no more frames" frames'
    let
        frame' = head'
        Cmog.SequenceFrame bursts' = frame'

    useShader log prog

    let _app = app & appMultiplyModel model'
        appmatrix = appMatrix _app
        scale' = sceneScale
        model' = multMatrices [ scaleX scale'
                              , scaleY scale'
                              , scaleZ scale' ]
        (model, view, proj) = map3 stackPop' appmatrix

    uniform log "model" umm =<< toMGC model
    uniform log "view" umv =<< toMGC view
    uniform log "proj" ump =<< toMGC proj

    let reducer acc burst =
            let Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst
            in  acc + length vertices'

    let burstsX' = zip bursts' buffers
        doPush = t == 0
    forM_ burstsX' $ \(burst, burstBuffer') ->
        drawSceneBurst log shader Nothing burstBuffer' burst doPush

drawSceneBurst log shader textureObjMb buffers burst doPush = do
    let shaderVars = shaderShaderVars shader
        ap  = shaderVars & attribAttribLocation   . shaderVarsMSAp
        atc = shaderVars & attribAttribLocation   . shaderVarsMSAtc
        an  = shaderVars & attribAttribLocation   . shaderVarsMSAn
        use = shaderVars & uniformUniformLocation . shaderVarsMSUse
        udc = shaderVars & uniformUniformLocation . shaderVarsMSUdc
        usc = shaderVars & uniformUniformLocation . shaderVarsMSUsc
        utt = shaderVars & uniformUniformLocation . shaderVarsMSUt

        Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst

        vert' = map cmogVertex3ToLocalVertex3 . DV.toList $ vertices'
        toNormalsMb = map (cmogVertex3ToLocalVertex4 1.0) . DV.toList

        [ vBuf, tcBuf, nBuf ] = buffers

        vAry  = bufferAry vBuf
        nAry  = bufferAry nBuf

        normals'   :: [Vertex4 Float]
        normals'   = maybe none' toNormalsMb normalsMb'
        none' = []
        len' = length vert'

        specularExp'   = Cmog.materialSpecularExp material' -- Float
        diffuseColor'  = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialDiffuseColor material'
        specularColor' = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialSpecularColor material'

    activateTextureMaybe log utt textureObjMb

    uniform log "specular exp"  use specularExp'
    uniform log "diffuse color"  udc diffuseColor'
    uniform log "specular color"  usc specularColor'

    if doPush then do
        pushPositionsWithArray log vAry ap (Just vert')
        pushNormalsWithArray log nAry an (Just normals')
    else do
        pushPositionsWithArray log vAry ap Nothing
        pushNormalsWithArray log nAry an Nothing

    attrib log "ap" ap Enabled
    attrib log "an" an Enabled

    wrapGL log "drawArrays" . drawArrays Triangles 0 . frint $ len'

    attrib log "an" an Disabled
    attrib log "ap" ap Disabled

    pure ()

drawWolf app wolfSeq shader textureMapping flipper t args = do
    let _app = app & appMultiplyModel model'
        log = appLog app
        flIsFlipping'
          | NotFlipping _ <- flipper = False
          | otherwise = True
        (flIsUpper', flIsLower')
          | flIsFlipping' = (False, False)
          | flHemisphere flipper == FlipUpper = (True, False)
          | otherwise = (False, True)
        flAngleDeg' = flAngleDeg flipper
        postRotateY'
            -- 0 at start, 180 for full flip.
          | flIsFlipping' = flAngleDeg'
          | flIsUpper' = 0
          | flIsLower' = 180
        postTranslateZ'
            -- 0 at start, 1.337 for full flip.
          | flIsFlipping' = flAngleDeg' / 180 * 1.337
          | flIsUpper' = 0
          | flIsLower' = 1.337
        model' = multMatrices [ scaleY 1.9
                              , scaleX 1.9
                              , scaleZ 1.9
                              , rotateY 45
                              , rotateX $ inv 90
                              , rotateY postRotateY'
                              , translateZ postTranslateZ'
                              , translateX $ inv 0.25
                              , translateZ 0.275 ]
        appmatrix    = appMatrix _app
        prog         = shaderProgram shader
        matrixVars   = shaderMatrix shader
        umm          = matrixVars & uniformUniformLocation . matrixVarsModel
        umv          = matrixVars & uniformUniformLocation . matrixVarsView
        ump          = matrixVars & uniformUniformLocation . matrixVarsProjection

        (model, view, proj) = map3 stackPop' appmatrix

        Cmog.Sequence frames' = wolfSeq

    -- infinite list => head is fine.
    head' <- headFail "drawScene: no more frames" frames'

    let Cmog.SequenceFrame bursts' = head'
        draw' = drawWolfBurst log shader

    useShader log prog
    uniform log "model" umm =<< toMGC model
    uniform log "view" umv =<< toMGC view
    uniform log "proj" ump =<< toMGC proj

    -- [fur (i.e. body), claws, eyes, teeth, mane=Material (i.e. fur), floor]
    let [tex1, tex2, tex3] = map graphicsTextureMappingTextureObject textureMapping
        (texFur, texEyes, texBody) = (tex1, tex2, tex3)
        [burstFur, _, burstEyes, _, burstMane, _] = bursts'

    mapM_ (draw' $ Just texBody) [burstFur]
    mapM_ (draw' $ Just texEyes) [burstEyes]

drawWolfBurst log shader textureObjMb burst = do
    let shaderVars      = shaderShaderVars shader
        ap              = shaderVars & attribAttribLocation . shaderVarsMAp
        atc             = shaderVars & attribAttribLocation . shaderVarsMAtc
        an              = shaderVars & attribAttribLocation . shaderVarsMAn
        use             = shaderVars & uniformUniformLocation . shaderVarsMUse
        uac             = shaderVars & uniformUniformLocation . shaderVarsMUac
        udc             = shaderVars & uniformUniformLocation . shaderVarsMUdc
        usc             = shaderVars & uniformUniformLocation . shaderVarsMUsc
        utt             = shaderVars & uniformUniformLocation . shaderVarsMUt
        uas             = shaderVars & uniformUniformLocation . shaderVarsMUas
        uss             = shaderVars & uniformUniformLocation . shaderVarsMUss

        Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst

        specularExp'   = Cmog.materialSpecularExp material'
        ambientColor'  = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialAmbientColor material'
        diffuseColor'  = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialDiffuseColor material'
        specularColor' = cmogVertex3ToLocalVertex4 1.0 $ Cmog.materialSpecularColor material'

        vert' = map cmogVertex3ToLocalVertex3 . DV.toList $ vertices'

        toTexCoordsMb = map (cmogVertex2ToLocalVertex4 0.0 1.0) . DV.toList
        toNormalsMb = map (cmogVertex3ToLocalVertex4 1.0) . DV.toList

        wolfTexCoords' :: [Vertex4 Float]
        wolfTexCoords' = maybe none' toTexCoordsMb texCoordsMb'
        wolfNormals'   :: [Vertex4 Float]
        wolfNormals'   = maybe none' toNormalsMb normalsMb'

        none' = []
        len' = length vert'

    activateTextureMaybe log utt textureObjMb

    uniform log "ambient strength" uas wolfAmbientStrength
    uniform log "specular strength" uss wolfSpecularStrength

    uniform log "specular exp"   use specularExp'
    uniform log "diffuse color"  udc diffuseColor'
    uniform log "specular color" usc specularColor'

    vPtr <- pushPositions log ap vert'
    tcPtr <- pushTexCoords log atc wolfTexCoords'
    nPtr <- pushNormals log an wolfNormals'

    attrib log "ap" ap Enabled
    attrib log "atc" atc Enabled
    attrib log "an" an Enabled
    wrapGL log "drawArrays" . drawArrays Triangles 0 . frint $ len'
    attrib log "an" an Disabled
    attrib log "atc" atc Disabled
    attrib log "ap" ap Disabled

    free nPtr
    free tcPtr
    free vPtr

activateTextureMaybe log utt textureObjMb = do
    let none' = do
            info log "[no texture]"
            pure ()
        activate' texObj = activateTexture log texObj utt
    maybe none' activate' textureObjMb

values :: Dmap.Map k v -> [v]
values = map snd . Dmap.toList

fs `asterisk` x = map map' fs where map' f = f x

drawStars :: App' -> (Shader', Shader') -> Int -> [String] -> IO ()
drawStars app (shaderC, shaderT) t args = do
    let star' = drawStar app (shaderC, shaderT) t args
    star' 2 4 0.2 0 0             $ hsvCycle 100 (t + 0)
    star' 1 6 0.1 45 90           $ hsvCycle 200 (t + 30)
    star' 1 8 0.05 (inv 45) 135   $ hsvCycle 300 (t + 50)
    star' 1 12 0.025 (inv 45) 135 $ hsvCycle 400 (t + 70)
    star' 1 20 0.025 (inv 45) 135 $ hsvCycle 500 (t + 90)
    star' 1 40 0.025 90 180       $ hsvCycle 600 (t + 120)

drawStar app (shaderC, shaderT) t args r velocity scale tilt initAngle hsv = drawStar' app' (shaderC, shaderT) hsv args where
    app'    = app & appMultiplyModel model'
    model' = multMatrices [ translateX (r / scale)
                          , rotateY (ry' + initAngle)
                          , rotateZ tilt
                          , scaleX scale
                          , scaleY scale
                          , scaleZ scale ]
    period' = floor $ 360 / velocity
    ry' = (* velocity) t'
    t' = frint $ t `mod` period'

drawStar' app (shaderC, shaderT) hsv args = do
    let shaderC' = ShaderDC (Just progc) ummc umvc umpc apc acc anc
        shaderT' = ShaderDT (Just progt) ummt umvt umpt utt apt att ant
        progc    = shaderProgram shaderC
        progt    = shaderProgram shaderT
        col'     = color cr' cg' cb' 255
        (cr', cg', cb') = hsv

        matrixVarsC = shaderMatrix shaderC
        matrixVarsT = shaderMatrix shaderT
        ummc        = matrixVarsC & uniformUniformLocation . matrixVarsModel
        umvc        = matrixVarsC & uniformUniformLocation . matrixVarsView
        umpc        = matrixVarsC & uniformUniformLocation . matrixVarsProjection
        ummt        = matrixVarsT & uniformUniformLocation . matrixVarsModel
        umvt        = matrixVarsT & uniformUniformLocation . matrixVarsView
        umpt        = matrixVarsT & uniformUniformLocation . matrixVarsProjection

        shaderVarsC = shaderShaderVars shaderC
        shaderVarsT = shaderShaderVars shaderT
        apc         = shaderVarsC & attribAttribLocation . shaderVarsCAp
        acc         = shaderVarsC & attribAttribLocation . shaderVarsCAc
        anc         = shaderVarsC & attribAttribLocation . shaderVarsCAn
        apt         = shaderVarsT & attribAttribLocation . shaderVarsTAp
        ant         = shaderVarsT & attribAttribLocation . shaderVarsTAn
        att         = shaderVarsT & attribAttribLocation . shaderVarsTAtc
        utt         = shaderVarsT & uniformUniformLocation . shaderVarsTUt
    drawStarSphere app shaderC' col' args
    forM_ [1 .. 4] $ \n -> drawStarCone app shaderC' col' args n 0
    forM_ [1,   3] $ \m -> drawStarCone app shaderC' col' args 1 m

drawStarSphere app shader' col args = sphere' where
    sphere' = sphere app' shader' (slices', stacks') col r
    app'    = app
    slices' = 5
    stacks' = 5
    r       = 1

drawStarCone app shader' col args n m = cone' where
    ty'        = inv 1.2
    height'    = 1.0
    numPoints' = 10

    cone'  = coneSection app' shader' numPoints' height' 0.001 0.5 0 (2 * pi) col
    app'   = app & appMultiplyModel model'
    n'     = frint n
    m'     = frint m
    model' = multMatrices [ rotateX 180
                          , translateY ty'
                          , rotateX $ 90 * n'
                          , rotateY $ 90 * m' ]

getMeshLimitFrames app = do
    let log = appLog app
        sdlGlesConfig = appConfig app
        config = app'Config app
    let numWolfFrames = config & configWolfFrames
    debug log $ "going to force deepseq with " ++ show numWolfFrames ++ " elements"
    let getLimitFrames'
          | ConfigWolfFramesNum n     <- numWolfFrames = pure $ take n
          | ConfigWolfFramesStr "all" <- numWolfFrames = pure id
          | ConfigWolfFramesStr s     <- numWolfFrames = die log $ printf "Invalid string value for number of wolf frames (%s)" s
    getLimitFrames'

initMeshes app textureConfigYaml' mtlObjSpec limitFramesMb = do
    let limitFrames = fromJust limitFramesMb
    let log = appLog app
        sdlGlesConfig = appConfig app
        config = app'Config app
    info log "Please wait . . . (parsing obj files, will take a while)"
    parsedEi' <- initMeshesParse log textureConfigYaml' mtlObjSpec
    info log "Done parsing"
    when (isLeft parsedEi') .
        dieM log $ printf "Unable to parse obj file: %s" (toLeft parsedEi')

    let (seq', texMap') = toRight parsedEi'
        meshSpec' = meshSpec app sdlGlesConfig texMap'
        no' = meshSpec' >>= \spec' -> pure (spec', seq')
        yes' limitFrames = do
            let Cmog.Sequence seqFrames' = seq'
                seq'' = Cmog.Sequence . limitFrames $ seqFrames'
            info log "getting it"
            spec' <- deepseq seq'' meshSpec'
            info log "gotting it"
            debug log "done forcing deepseq"
            pure (spec', seq'')

    (spec', seq'') <- maybe no' yes' limitFramesMb
    pure (spec', Just seq'')

    -- deepseq in combination with our limitFrames
    -- function works correctly: it only forces the
    -- chunk which results from limitFrames.
    -- info log $ "spec' length: " ++ length spec'
    -- pure (spec', Just seq')

initBuffers :: [Cmog.Burst] -> [BufferMaker] -> IO [[Buffer]]
initBuffers bursts attributeConstructors = do
    let print' burst = do
            let (vl, tcl) = analyzeBurst burst
            putStrLn $ printf "vertex: %d, tex: %d, norm: %d" vl tcl vl
    when True $ mapM_ print' bursts

    mapM (makeBuffers attributeConstructors) bursts

-- position (num vertices)
-- texcoords (num texcoords)
-- normals (num vertices)
-- and all other attributes are tied to num vertices.

makeBuffers attributeConstructors burst = do
    let (vl, tcl) = analyzeBurst burst
        map' (len', maker') = makeBuffer len' maker'
        x = vl : tcl : repeat vl

    mapM map' $ zip x attributeConstructors

makeBuffer len maker = Buffer len' <$> mallocArray len' where
    len' = len * factor'
    factor' = case maker of MakeBufferScalar -> 1
                            MakeBufferVertex3 -> 3
                            MakeBufferVertex4 -> 4

-- assumption: it is possible that there are fewer texture coordinates
-- entries than vertices, but always an equal number of normals.
analyzeBurst burst = (vertexLength, texCoordLength) where
    Cmog.Burst vertices' texCoordsMb' normalsMb' material' = burst
    vertexLength   = numVertices'
    texCoordLength = length (maybe [] DV.toList texCoordsMb')
    numVertices'   = length vertices'

die log str = do
    _ <- err log str
    error str

dieM log str = err log str >> empty

cmogVertex3ToLocalVertex3     (Cmog.Vertex3 a b c) = Vertex3 a b c
cmogVertex2ToLocalVertex4 c d (Cmog.Vertex2 a b  ) = Vertex4 a b c d
cmogVertex3ToLocalVertex4 d   (Cmog.Vertex3 a b c) = Vertex4 a b c d

sequence2 (a, b) = tuple2 <$> a <*> b
tuple2 a b = (a, b)

torus app shaderC innerRadius thickness ncylinders = do
    let torus'' = torus' app shaderC innerRadius thickness ncylinders
    forM_ [0 .. ncylinders - 1] torus''

torus' app shaderC innerRadius thickness ncylinders n = do
    let app' = app & appMultiplyModel model'
        n' = frint n
        dt = 360 / frint ncylinders
        col = col' $ hsvCycle ncylinders n
        col' (r, g, b) = color r g b 255
        h = 2 * pi * innerRadius / frint ncylinders
        model' = multMatrices [ rotateZ 90
                              , translateZ innerRadius
                              , rotateY $ dt * n' ]
    cylinder app' shaderC 20 h (thickness * 2) 0 (2 * pi) col

readFileBS :: Loggers -> String -> String -> IO ByteString
readFileBS loggers hint path = BS.readFile path <|> die' where
    die' = piep' *> empty
    piep' = warn $ printf "Unable to open %s (%s)" path hint
    warn = loggersWarn loggers

loggersInfo = fst3
loggersWarn = snd3
loggersError = thd3
(f .*> g) x = f x *> g x
