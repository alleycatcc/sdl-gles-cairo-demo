{-# LANGUAGE OverloadedStrings #-}

module Graphics.SGCDemo.ConfigInline ( configInline ) where

import           Data.ByteString ( ByteString )

configInline :: ByteString
configInline =
    -- dummy width and height: the real width & height are passed in after
    -- being measured.
    "viewportWidth: 480\n" <>
    "viewportHeight: 793\n" <>

    -- possible values:
    --   extreme | simple | onecat | twocats | fourcats | eightcats |
    --   eightbubbles | onemovie | twomovies | nomovie | eightmovies |
    --   vikingplaza | eightpics | galerie
    "faceSpec: eightcats\n" <>

    "doWolf: false\n" <>
    "doScene: false\n" <>
    "sceneScale: 0.1\n" <>
    "numWolfFrames: 1\n" <>
    "doInitRotate: true\n" <>
    "doCube: true\n" <>
    "cubeFacesInner: [0, 1, 2, 3]\n" <>
    "cubeFacesOuter: [0, 1, 2, 3]\n" <>
    "doCarrousel: false\n" <>
    "doStars: false\n" <>
    "doTorus: false\n" <>
    "doBackground: true\n" <>
    "doTransformTest: false\n" <>
    "doHoop: false\n" <>
    "hoopVelocityX: 0.8\n" <>
    "hoopVelocityY: 0.1\n" <>
    "hoopColor: 888811ff\n"
