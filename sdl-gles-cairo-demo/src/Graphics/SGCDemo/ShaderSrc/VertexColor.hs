module Graphics.SGCDemo.ShaderSrc.VertexColor ( shaderSrc
                                              ) where

shaderSrc =
    "#version 100\n" ++
    "uniform mat4 model;\n" ++
    "uniform mat4 view;\n" ++
    "uniform mat4 projection;\n" ++
    "attribute vec4 a_position;\n" ++
    "attribute vec4 a_color;\n" ++
    "varying vec4 v_color;\n" ++
    "void main()\n" ++
    "{\n" ++
    "   v_color = a_color;\n" ++
    "   gl_Position = projection * view * model * a_position;\n" ++
    "}\n" ++
    "\n"

