module Graphics.SGCDemo.ShaderSrc.FragmentColor ( shaderSrc
                                                ) where

shaderSrc =
    "#version 100\n" ++
    "#ifdef GL_ES\n" ++
    "precision mediump float;\n" ++
    "#endif\n" ++
    "varying vec4 v_color;\n" ++
    "void main()\n" ++
    "{\n" ++
    "   gl_FragColor = v_color;\n" ++
    "}\n"

