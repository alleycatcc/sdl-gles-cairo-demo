module Graphics.SGCDemo.ShaderSrc.VertexMesh ( shaderSrc
                                             ) where

shaderSrc =
    "#version 100\n" ++
    "// ES 2.0 requires 100 or 300, which are the ES versions.\n" ++
    "\n" ++
    "// To use this shader, each mesh must have a texture.\n" ++
    "\n" ++
    "uniform mat4 model;\n" ++
    "uniform mat4 view;\n" ++
    "uniform mat4 projection;\n" ++
    "\n" ++
    "uniform mat4 transpose_inverse_model;\n" ++
    "\n" ++
    "attribute vec4 a_position;\n" ++
    "attribute vec2 a_texcoord;\n" ++
    "attribute vec4 a_normal;\n" ++
    "\n" ++
    "attribute float specularExp;\n" ++
    "attribute vec4 ambientColor;\n" ++
    "attribute vec4 diffuseColor;\n" ++
    "attribute vec4 specularColor;\n" ++
    "\n" ++
    "varying vec4 v_position;\n" ++
    "varying vec2 v_texcoord;\n" ++
    "varying vec4 v_normal;\n" ++
    "\n" ++
    "// varying float v_specularExp;\n" ++
    "// varying vec4 v_ambientColor;\n" ++
    "// varying vec4 v_diffuseColor;\n" ++
    "// varying vec4 v_specularColor;\n" ++
    "\n" ++
    "void main()\n" ++
    "{\n" ++
    "    gl_Position = projection * view * model * a_position;\n" ++
    "    v_texcoord = a_texcoord;\n" ++
    "\n" ++
    "    // -- eye-space.\n" ++
    "    v_position = view * model * a_position;\n" ++
    "\n" ++
    "    vec4 normal = vec4 (vec3 (a_normal), 0.0);\n" ++
    "\n" ++
    "    // v_normal = view * transpose_inverse_model * normal;\n" ++
    "    v_normal = view * model * normal;\n" ++
    "\n" ++
    "    // v_specularExp = specularExp;\n" ++
    "    // v_ambientColor = a_ambientColor;\n" ++
    "    // v_diffuseColor = a_diffuseColor;\n" ++
    "    // v_specularColor = a_specularColor;\n" ++
    "}\n"

