#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
framesdir="$bindir"/frames

framesresizedir_parts=("$bindir" frames-resize)
framesbase64dir_parts=("$bindir" frames-base64)

. "$bindir"/functions.bash

framesresizedir=$(join-out / framesresizedir_parts)
framesbase64dir=$(join-out / framesbase64dir_parts)

USAGE="Usage: $0"

go () {
    fun safe-rm-dir-array-allow-absolute framesresizedir_parts
    fun safe-rm-dir-array-allow-absolute framesbase64dir_parts
    mkd "$framesresizedir"
    mkd "$framesbase64dir"

    local base
    for i in "$framesdir"/*; do
        cmd-capture _ret0 basename "$i"
        base="$_ret0"
        cmd convert "$i" -resize 512x512 "$framesresizedir"/"$base"
        fun redirect-out "$framesbase64dir"/"$base" base64 -w0 "$framesresizedir"/"$base"
        # fun redirect-out "$framesbase64dir"/"$base" base64 -w0 "$i"
    done
}

fun go
