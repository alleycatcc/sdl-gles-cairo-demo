module Main where

import           System.Environment                 ( getArgs )
import qualified Data.ByteString.Base64     as B64  ( encode )
import           Text.Printf ( printf )
import           Data.ByteString            as BS   ( ByteString
                                                    , readFile )
import           System.Console.Chalk ( bold, blue, red  )
import           Graphics.SGCDemo.Launch ( launch, launchWithArgs )

type Log = String -> IO ()

main :: IO ()
main = do
    args <- getArgs
    launchWithArgs (info, warn, err) Nothing args where
        info = log' $ blue "٭"
        warn = log' $ (bold . red) "٭" <> " Warning:"
        err = log' $ red "٭" <> " Error:"
        log' x = putStrLn . printf "%s %s" x
