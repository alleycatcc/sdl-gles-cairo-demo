#!/bin/bash

set -eu
set -o pipefail

_ret=

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

projname=sdl-gles-cairo-demo
rootdir="$bindir"/..
hssrcroot="$rootdir"/"$projname"
sourcesdirparts=("$rootdir" sources)
sourcesdir=$(join-out / sourcesdirparts)
imagename=$projname
tarfile="$sourcesdir"/haskell-module-src.tar

# --- haskell-android-sdl uses the old-style (v1) cabal builds, and only
# gets packages from hackage, while our packages aren't (all) on hackage.
# so we just take the packages from git and place them in a folder where the
# v1-style build can see them and use them.
sdlglescairoremote=https://gitlab.com/alleycatcc/sdl-gles-cairo
sdlglescairorevision=2dbe996131a2dd8a95602eb14a94fff95da5b4ff
meshobjglesremote=https://gitlab.com/alleycatcc/mesh-obj-gles
meshobjglesrevision=c318f2878b9c3c6ee337aa8ff3cf625bdf3086b0
wavefrontzremote=https://gitlab.com/alleycatcc/wavefront
wavefrontzrevision=61fa78beca53cdb30cff4ecff8038e169d443f7a
sdlcairodemocatremote=https://gitlab.com/alleycatcc/sdl-cairo-demo-cat
sdlcairodemocatrevision=b82e03c1ba802eab547a04aecdff8527fa28558f

USAGE="Usage: $0 { [-C] [-I] build | [-m mount-dir] build-apk-and-deploy | [-m mount-dir] run-interactive}

build will build the Haskell sources (which is the brunt of the work) and
make a Docker image.

build-apk-and-deploy will run the Docker image, which will build the Java
sources, build the .apk and deploy it.

-C is to disable the Docker cache, which can be useful to uncache commands
like e.g. 'apt update'

-I is to not initialise the haskell sources, in order to take advantage of
the docker cache. This is useful for example when only changing the Java
code. In reality though the docker cache still tends to break on the COPY
commands, meaning the haskell sources still end up being rebuilt, but
there's probably a way to fix this.

mount-dir is where the built .apk will be placed."

opt_C=
opt_m=
opt_I=
while getopts hm:IC-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        m) opt_m="$OPTARG" ;;
        I) opt_I=yes ;;
        C) opt_C=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

if [ $# -lt 1 ]; then
    error "$USAGE"
fi

mode=$1; shift

init-sources () {
    local s
    local first=yes
    local flag

    fun safe-rm-dir-allow-absolute ${sourcesdirparts[@]}
    mkd "$sourcesdir"
    chd "$sourcesdir"

    cmd git clone "$sdlglescairoremote" sdl-gles-cairo
    cwd sdl-gles-cairo git reset --hard "$sdlglescairorevision"

    cmd git clone "$meshobjglesremote" mesh-obj-gles
    cwd mesh-obj-gles git reset --hard "$meshobjglesrevision"

    cmd git clone "$wavefrontzremote" wavefrontz
    cwd wavefrontz git reset --hard "$wavefrontzrevision"

    cmd git clone "$sdlcairodemocatremote" sdl-cairo-demo-cat
    cwd sdl-cairo-demo-cat git reset --hard "$sdlcairodemocatrevision"

    cmd ln -s "$hssrcroot"

    cmd rm -f "$tarfile"

    mci
    mcb tar cvf "$tarfile"
    mcb   -h
    mcb   --exclude=*.tar
    mcb   --exclude=.git
    mcb   --exclude=dist
    mcb   --exclude=dist-newstyle
    mcb   .
    mcg

    chd-back
}

docker-build () {
    local nocache=$1; shift
    local opt_nocache=
    if [ "$nocache" = yes ]; then
        opt_nocache=--no-cache
    fi
    cmd docker build $opt_nocache "$rootdir" -t "$imagename" -f docker/Dockerfile "$@"
}

get-mount-opt () {
    local ret=$1; shift
    local mountdir=${1:-}; shift
    local mountopt=''
    if [ -n "$mountdir" ]; then
        mountopt="-v $mountdir:/media/shared"
    fi
    retvar "$ret" "$mountopt"
}

docker-build-apk-and-deploy () {
    local mountdir=${1:-}; shift
    fun get-mount-opt _ret "$mountdir"
    local mountopt="$_ret"
    cmd docker run --privileged -it -v /dev/bus/usb:/dev/bus/usb $mountopt "$@" "$imagename":latest
}

docker-run-interactive () {
    local mountdir=${1:-}; shift
    fun get-mount-opt _ret "$mountdir"
    local mountopt="$_ret"
    cmd docker run --privileged -it -v /dev/bus/usb:/dev/bus/usb $mountopt "$@" "$imagename":latest /bin/bash
}

if [ "$mode" = build ]; then
    if [ ! "$opt_I" = yes ]; then
        fun init-sources
    fi
    fun docker-build "$opt_C" "$@"
elif [ "$mode" = build-apk-and-deploy ]; then
    fun docker-build-apk-and-deploy "$opt_m" "$@"
elif [ "$mode" = run-interactive ]; then
    fun docker-run-interactive "$opt_m" "$@"
else error "$USAGE"; fi
